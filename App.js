import React from 'react';
import  {View, ActivityIndicator} from "react-native";
import Header from "./src/components/Header/Header";
import ArticlePage from "./src/components/ArticlesPage/ArticlesPage";
import CategoriesPage from "./src/components/CategoriesPage/CategoriesPage";
import SingleArticle from "./src/components/SingleArticlePage/SingleArticle";
import { createStackNavigator } from 'react-navigation';
import * as Font from 'expo-font';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import reducers from "./src/reducers/index";

const RootStack = createStackNavigator(
    {
        Article: {screen: SingleArticle},
        Articles: { screen: ArticlePage },
        Catigories: { screen: CategoriesPage },
    },
    {
        initialRouteName: 'Articles',
    },
);

const store = createStore(combineReducers(reducers), applyMiddleware(thunk));

export default class App extends React.Component {
  constructor() {
    super()
    this.state = {
      fontsLoaded: false,
    }
  }

 async componentDidMount(){
    await Font.loadAsync({
      "AbrilFatface-Regular":require("./assets/fonts/AbrilFatface-Regular.ttf"),
      "PlayfairDisplay-Regular":require("./assets/fonts/PlayfairDisplay-Regular.ttf")
    });
    this.setState({fontsLoaded:true});
  }

  render() {
    return (
        <Provider store={store}>
            <View style={{flex:1, justifyContent:"center"}}>
                {this.state.fontsLoaded ? (<RootStack />) : (<ActivityIndicator size="large"/>)}
            </View>
        </Provider>
    );
  }
}