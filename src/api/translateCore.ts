import axios from 'axios';
import Register from "./register"
import { IMPORT_LANGUAGES } from "../actions/actionTypes";

const url: string = Register.createServiseUrl("translate");

interface ITranslateParams {
    sourceLanguage: string,
    targetLanguage: string,
    text: string
}

export function translateWord(params: ITranslateParams) {
    // params.text = encodeURI(params.text);
    const queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');

    return axios.get(`${url}/google/?${queryString}`)
    .then(translate => {
        return translate.data;
    })
    .catch(error => {
        console.log(error);
    });
}

export function getLanguagesList ():any {
return dispatch => axios.get(`${url}/languages`)
    .then (lang => {
        return lang.data;
    })
    .then(languages => dispatch({
        type: IMPORT_LANGUAGES,
        languages
    }))
    .catch (error => {
        console.log(error);
    })
}