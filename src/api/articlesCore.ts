import axios from 'axios';
import Register from "./register"

const url = Register.createServiseUrl("portal");

export function getArticles() {
  console.log(url);
  return axios.get(`${url}/nytimes/`)
  .then(responseJson => {
    return responseJson.data;
  })
  .catch(error => {
    console.error(error);
  });
}

export function getArticleById (id) {
  return axios.get(`${url}/nytimes/${id}`)
    .then(responseJson => {
      return responseJson.data;
    })
    .catch(error => {
      console.log(error);
    })
}