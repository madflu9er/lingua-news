interface IHost {
    protocol: string;
    host: string;
    port: string;
    context: string;
}


export default class Register {
    static UpdateRegister () {
        //code to upate register;
    }

    static portal: IHost = {
        protocol: "https",
        host: "linguagenews.herokuapp.com",
        port: "443",
        context: "/portal"
    }

    static translate: IHost = {
        protocol: "https",
        host: "translate-linguanews-app.herokuapp.com",
        port: "443",
        context: "/translate"
    }

    //translate service;

    static createServiseUrl (serviceName: string): string {
        const service: IHost = this.getHostByName(serviceName);
        return service !== null ? `${service.protocol}://${service.host}:${service.port}${service.context}` : null;
    }

    static getHostByName(name: string): IHost {
        switch (name) {
            case "portal":
                return Register.portal;
            case "translate":
                return Register.translate;
            default: null;
        }
    }
}