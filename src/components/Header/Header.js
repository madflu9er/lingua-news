import React, {Component} from "react";
import {TouchableOpacity, View, StyleSheet, Text, TextInput, Button, Image} from "react-native";

export default class Header extends Component {
    constructor(props){
        super(props);
    }

    render () {
        return(
            <View style={styles.headerView}>
                <TouchableOpacity style={styles.touchable}>
                    <View style={styles.view}>
                        <Image
                        source={require("./pictures/dictionary_white.png")}
                        style={styles.image}
                        />
                    </View>
                </TouchableOpacity>
                <TextInput style={styles.headerSearchInput} placeholder="Type to search article" placeholderTextColor="white"/>
                <TouchableOpacity style={styles.touchable}>
                    <View style={styles.view}>
                        <Image
                        source={require("./pictures/menu_white.png")}
                        style={styles.image}
                        />
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    headerView: {
        height: "10%",
        backgroundColor: "#45645F",
        alignItems: "center",
        justifyContent: "space-around",
        flexDirection: "row",
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
	    borderTopLeftRadius: 0,
        borderTopRightRadius: 0,
    },
    headerSearchInput: {
        height: "65%",
        width: "75%",
        borderColor: 'white',
        color: "white",
        borderWidth: 1.3,
        borderRadius: 5,
    },
    searchButton: {
        width: 50,
        height: 35,
        borderWidth: 2,
        borderColor: "black"
    },
    view: {
        width: "100%",
        height: "100%",
    },
    image: {
        height: "100%",
        resizeMode: "center",
        width: "100%"
    },
    touchable: {
        alignItems: 'center',
        justifyContent: 'center',
        width: "10%",
        paddingLeft: 2,
        paddingRight: 2,
        height: "100%"
    }
});