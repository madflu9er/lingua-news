import React, {Component} from 'react';
import {View, StyleSheet, ScrollView, ActivityIndicator, Text} from 'react-native';
import { withNavigation } from "react-navigation";

import Header from "../Header/Header";
import ArticleListItem from "../ArticleListItem/ArticleListItem";
import { getArticles } from "../../api/articlesCore";
import { getLanguagesList } from "../../api/translateCore"
import HiddenMenu from '../HiddenMenu/HiddenMenu';
import { connect } from 'react-redux';


class ArticlePage extends Component {
    constructor(props) {
        super(props);
        this.state={
            articlesLoad: false,
            articles: null
        }
      }
      static navigationOptions = {
        title: "Articles",
        headerStyle: {
          backgroundColor: "#45645F",
          elevation: 0,
          borderBottomWidth: 0,
        },
        headerTitleStyle: { color: 'white' },
      };

      async componentDidMount(){
        const {loadLanguages} = this.props;
        loadLanguages();
        let articles = await getArticles();
        this.setState({articlesLoad:true, articles});
      }

      render() {
        const { language, languagesList } = this.props;
        const { navigate } = this.props.navigation;
        const { articles } = this.state;
        const goToArticle = (a) => navigate('Article', {article: a});
        return (
        <View style={styles.mainScreenView}>
            <HiddenMenu content={languagesList}/>
            {this.state.articlesLoad ? (
                <View style={{flex:1, justifyContent:"center"}}>
                    <Header />
                    {
                        Array.isArray(this.state.articles) ? (
                            <ScrollView style={styles.articlesWrapper}>
                                {articles && articles.map((article)=>(
                                    <ArticleListItem key={article.id} onClick={goToArticle} title={article.title} general={article.abstract} article={article}/>
                                ))}
                            </ScrollView>
                        ) : (
                            <View style={styles.errorMessage}>
                                <Text>{(articles.status)}</Text>
                            </View>
                        )
                    }
                </View>
            ) : (
                <ActivityIndicator size="large"/>
            )}
        </View>
        );
      }

}
const styles = StyleSheet.create({
    settings: {
        position: "absolute",
        top: 0,
        left: 0,
        width: "90%",
        height: "100%",
        backgroundColor: "grey",
        zIndex: 1,
        opacity: 0.9
    },
    mainScreenView: {
      height: "100%",
      backgroundColor: "white",
      justifyContent: "center"
    },
    articlesWrapper: {
        height: "90%",
        width: "100%",
        flexDirection: "column",
    },
    errorMessage: {
        height: "90%",
        width: "100%",
        flexDirection: "column",
        justifyContent:"center",
        alignItems:"center"
    }
});

const mapDispatchToProps = dispatch => ({
    loadLanguages: () => dispatch(getLanguagesList())
  });

const mapStateToProps = state => ({
    language: state.menu.language,
    languagesList: state.menu.languagesList
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(withNavigation(ArticlePage));