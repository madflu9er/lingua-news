import React, {Component} from 'react';
import {View, StyleSheet, Button} from 'react-native';

export default class CategoriesPage extends Component {
    constructor(props) {
        super(props);
      }
      render() {
        const {onCategoryClick} = this.props
        return (
            <View style={styles.mainScreenView}>
                <Button title="New category" onPress={() => onCategoryClick('Articles')}/>
            </View>
        );
      }

}
const styles = StyleSheet.create({
    mainScreenView: {
        height: "90%",
        backgroundColor: "#ffffff",
      }
});