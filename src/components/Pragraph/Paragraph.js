import React, {Component} from "react";
import {View, StyleSheet} from "react-native";
import Word from "../Word/Word"

export default class Paragraph extends Component {
    constructor(props) {
        super(props);
      }

      render() {
        const { paragraph } = this.props;
        return (
            <View style={styles.paragraph}>
                {paragraph && paragraph.words.map(word => (
                    <Word word={word.word} key={word.key} />
                ))}
            </View>
        );
      }

}
const styles = StyleSheet.create({
    paragraph: {
        width: "100%",
        display: "flex",
        alignItems:"flex-start",
        flexDirection: "row",
        flexWrap: "wrap",
        justifyContent: "flex-start",
        paddingTop: 15,
        paddingRight: 10,
        paddingLeft: 10,
        paddingBottom: 15,
    },
    articleTitleWrapper: {
        borderBottomWidth: 2,
        borderBottomColor: "black",
        width: "90%"
    },
    articleTitle: {
        textAlign: "left",
        color: "black",
        marginBottom: 5,
        fontSize: 30,
        fontFamily: "AbrilFatface-Regular"
    },
    articleGeneralWrapper:{
        marginTop: 5,
        paddingLeft: 15,
        paddingRight: 15,
        width: "100%",
    },
    articleGeneral: {
        textAlign: "left",
        color: "black",
        fontSize: 16,
        fontFamily: "PlayfairDisplay-Regular"
    }
});