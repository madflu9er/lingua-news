import React, {Component} from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import {withNavigation} from "react-navigation";

import Category from "../Category/Category";
import { categories } from "../CategoriesPage/categoriesPageCore"


class CategoriesPage extends Component {
    constructor(props) {
        super(props);

      }
      static navigationOptions = {
        title: "Catigories",
        headerStyle: {
          backgroundColor: "#ccc"
        }
      };


    render() {
        const { navigate } = this.props.navigation;
        const navigateToArticles = (category) => navigate('Articles', {acticleCategory:  category });

        return (
            <ScrollView style={styles.container}>
                {categories.map(category => (
                    <Category name={category.name} key={category.key} onCategoryClick={navigateToArticles}/>   
                ))}
            </ScrollView>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: "100%",
        flex: 1
    }
});

export default withNavigation(CategoriesPage)