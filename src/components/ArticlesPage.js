import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import { withNavigation } from "react-navigation";

class ArticlePage extends Component {
    constructor(props) {
        super(props);
        
      }
      static navigationOptions = {
        title: "Articles",
        headerStyle: {
          backgroundColor: "#ccc"
        }
      };
      render() {
        const {navigation} = this.props;
        const { articleName } = this.props
        return (
        <View style={styles.mainScreenView}>
            <Text>{navigation.getParam('articleName', 'Noname')}</Text>
        </View>
        );
      }

}
const styles = StyleSheet.create({
    mainScreenView: {
      height: "100%",
      backgroundColor: "#ffffff",
    }
});

export default withNavigation(ArticlePage);