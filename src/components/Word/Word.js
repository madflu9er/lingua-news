import React, {Component} from "react";
import {TouchableOpacity, View, StyleSheet, Text} from "react-native";
import { translateWord } from "../../api/translateCore";
import { connect } from 'react-redux';

class Word extends Component {
    constructor(props) {
        super(props);
        this.state={
            defaultWord: props.word,
            translatedWord: null,
            isTranslated: false
        }
      }

      async translate () {
          const { word, language } = this.props;
          const { translatedWord, defaultWord } = this.state;

          if (translatedWord) {
              this.setState({
                  isTranslated: !this.state.isTranslated
              })
          } else {
            params = {
                sourceLanguage: "EN",
                targetLanguage: language,
                text: encodeURI(word)
            }
            let translateRes = await translateWord(params);
            this.setState({
                translatedWord: translateRes.translate ? translateRes.translate : defaultWord,
                isTranslated: true
            })
        }
      }

      render() {
        const { word } = this.props;
        const { defaultWord, translatedWord, isTranslated } = this.state;
        return (
            <View>
                <TouchableOpacity onPress={() => { this.translate(); }}>
                    <Text style={styles.word}>{isTranslated ? translatedWord : defaultWord}</Text>
                </TouchableOpacity>
            </View>
        );
      }

}
const mapStateToProps = state => ({
    language: state.menu.language,
});

export default connect(
    mapStateToProps,
)(Word);

const styles = StyleSheet.create({
    word: {
        fontSize: 20,
        fontFamily: "PlayfairDisplay-Regular",
        paddingRight: 5
    }
});