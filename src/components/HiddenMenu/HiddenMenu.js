import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';


export default class HiddenMenu extends Component {
    constructor(props) {
        super(props);
      }

      render() {
        const { content } = this.props;
        return (
        <View style={styles.hiddenMenu}>
            {content.map( item => (
                <Text>{item}</Text>
            ))}
        </View>
        );
      }

}

const styles = StyleSheet.create({
    hiddenMenu: {
        position: "absolute",
        top: 0,
        left: 0,
        width: "90%",
        height: "100%",
        backgroundColor: "grey",
        zIndex: 1,
        opacity: 0.9
    }
});