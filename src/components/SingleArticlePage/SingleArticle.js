import React, {Component} from 'react';
import {Text, StyleSheet, View, ScrollView, ActivityIndicator} from 'react-native';
import {withNavigation} from "react-navigation";

import { getArticleById } from "../../api/articlesCore"
import Paragraph from "../Pragraph/Paragraph";


class SingleArticle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            color: "black",
            loading: false,
            singleArticle: null
        }
      }

      async componentDidMount() {
        const article = this.props.navigation.getParam('article');
        let singleArticle = await getArticleById(article.id);
        this.setState({singleArticle: singleArticle, loading: true})
      }

      static navigationOptions = ({ navigation, screenProps }) => ({
        title: navigation.getParam('article').title,
        headerStyle: {
          backgroundColor: "#45645F"
        },
        headerTitleStyle: { color: 'white' },
      });


    render() {
        // const article = this.props.navigation.getParam('article');
        const { singleArticle } = this.state;
        return (
            <View style={styles.container}>
                {this.state.loading ? (
                <ScrollView style={{flex:1}}>
                    <Text style={styles.articleHeader}>{singleArticle ? singleArticle.title : ""}</Text>
                    {singleArticle ? singleArticle.paragraphs.map(p => (
                        <Paragraph paragraph={p} key={p.key} />
                    )) : "DIDNT GET DATA"}
                </ScrollView>
            ) : (
                <ActivityIndicator size="large"/>
            )}
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: "100%",
        justifyContent:"center",
        flex: 1
    },
    articleHeader: {
        textAlign: "left",
        color: "black",
        marginBottom: 5,
        marginTop: 5,
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 28,
        fontFamily: "AbrilFatface-Regular"
    }
});

export default withNavigation(SingleArticle)