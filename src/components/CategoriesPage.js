import React, {Component} from 'react';
import {View, StyleSheet, Button} from 'react-native';
import Header from "./Header";
import Categories from "./Categories";
import {withNavigation} from "react-navigation"

class CategoriesPage extends Component {
    constructor(props) {
        super(props);

      }
      static navigationOptions = {
        title: "Catigories",
        headerStyle: {
          backgroundColor: "#ccc"
        }
      };
      render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                    <Header />
                    <Button title="New category" onPress={() => navigate('Articles', {articleName: 'New one' })}/>
            </View>
        );
      }

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
    }
});

export default withNavigation(CategoriesPage)