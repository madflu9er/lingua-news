import React, {Component} from 'react';
import {View, StyleSheet, Button, TouchableOpacity, Text} from 'react-native';

export default class ArticleListItem extends Component {
    constructor(props) {
        super(props);
      }
      
      render() {
        const {onClick, title, general, article} = this.props
        return (
        <TouchableOpacity style={styles.article} onPress={() => {onClick(article)}}>
            <View style={styles.articleTitleWrapper}>
                <Text style={styles.articleTitle}>{title}</Text>
            </View>
            <View style={styles.articleGeneralWrapper}>
                <Text style={styles.articleGeneral}>{general}</Text>
            </View>
        </TouchableOpacity>
        );
      }

}
const styles = StyleSheet.create({
    article: {
        width: "100%",
        backgroundColor: "white",
        marginTop: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
	    borderTopLeftRadius: 0,
        borderTopRightRadius: 0,
        shadowOffset:{  width: 0,  height: 1,  },
        shadowColor: 'black',
        shadowOpacity: 0.5,
        alignItems:"center",
        paddingTop: 15,
        paddingBottom: 15,
        elevation: 5,
    },
    articleTitleWrapper: {
        borderBottomWidth: 2,
        borderBottomColor: "black",
        width: "90%"
    },
    articleTitle: {
        textAlign: "left",
        color: "black",
        marginBottom: 5,
        fontSize: 30,
        fontFamily: "AbrilFatface-Regular"
    },
    articleGeneralWrapper:{
        marginTop: 5,
        paddingLeft: 15,
        paddingRight: 15,
        width: "100%",
    },
    articleGeneral: {
        textAlign: "left",
        color: "black",
        fontSize: 16,
        fontFamily: "PlayfairDisplay-Regular"
    }
});