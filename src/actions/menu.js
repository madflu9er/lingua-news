import { CHANGE_LANGUAGE } from "./actionTypes";

export function onLanguageChange(language) {
    return dispatch => dispatch({
        type: CHANGE_LANGUAGE,
        language
    });
}