import { CHANGE_LANGUAGE, IMPORT_LANGUAGES } from "../actions/actionTypes";

const INITIAL_STATE = {
    language: "RU",
    languagesList: []
};

export default function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case CHANGE_LANGUAGE:
        return { ...state,
            language: action.language
        };
        case IMPORT_LANGUAGES:
            return {
            ...state,
            languagesList: action.languages
            }
        default: return state;
    }
  }